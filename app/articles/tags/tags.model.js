const mongoose = require('mongoose');

const tagOpt = {
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 35,
    trim: true,
    unique: true
  },
  mainSearchTag:{
    type: Boolean,
    required: true,
    default: false,
  }
};

var ArticleTags = mongoose.model('ArticleTag' , tagOpt );

module.exports = { ArticleTags };
