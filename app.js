// const helmet = require('helmet'); //security
// app.use(helmet());

const express = require('express');
const bodyParser = require('body-parser');
const { mongoose } = require('./server/mongoosedb');


const app = express();
const path = require("path");
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin" , "*");
  res.header("Access-Control-Allow-Headers" , "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

// article controller
require('./app/articles/articles.controller.js')(app);
// users controller
require('./app/users/users.controller.js')(app);
// shop clap design controller
require('./app/shop-design-claps/design-claps.controller.js')(app);

app.listen(process.env.PORT , () => {
  console.log('started at ', process.env.PORT );
});
