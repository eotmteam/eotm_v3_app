const jwt = require('jsonwebtoken')
const {User} = require('./users.model.js')
const config = require("./auth.config.json");

const auth = async(req, res, next) => {
  console.log("auth current user token");
  try {
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token, config.eotmjwtkey)
    const user = await User.findOne({ _id: data._id, 'tokens.token': token })
    if (!user) {
      throw new Error()
    }
    req.user = user
    req.token = token
    next()
  } catch (error) {
    res.status(401).send({ error: 'Not authorized to access this resource' })
  }
}

// const authAdmin = async(req, res, next) => {
//   console.log("auth current user token + isAdmin");
//   try {
//     const token = req.header('Authorization').replace('Bearer ', '')
//     const data = jwt.verify(token, config.eotmjwtkey)
//     const user = await User.findOne({ _id: data._id, 'tokens.token': token })
//     if ( !user || ( user && !user.isAdmin ) ) {
//       throw new Error()
//     }
//     req.user = user
//     req.token = token
//     next()
//   } catch (error) {
//     res.status(401).send({ error: 'Not authorized to access this resource with non Admin user' })
//   }
// }

// const auth = { user: authUser, admin:authAdmin  }

module.exports = auth
