
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const dbOptions =  { useNewUrlParser: true, useCreateIndex: true , useUnifiedTopology: true };
mongoose.connect(process.env.DATABASE, dbOptions);

module.exports = {mongoose};
