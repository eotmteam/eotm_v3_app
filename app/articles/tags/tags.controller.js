
// const { ArticleTagsService } = require('./tags.service.js')


module.exports = function( app, ArticleTagsService ){

  app.get('/tags/all', function(req, res){
    ArticleTagsService.get().then((_tags)=> {
      res.send(_tags);
    }, (err) => {
      console.log('error -> ', err);
      res.send(err);
    })
  });


  app.get('/tags/search/:query', function(req, res){

    // console.log('search q ', req.params.query);

    ArticleTagsService.search(req.params.query).then((_tags)=> {
      res.send(_tags);
      console.log('search tags ' );
    }, (err) => {
      console.log('error -> ', err);
      res.send(err);
    })
  });


}
