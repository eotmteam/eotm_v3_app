const mongoose = require('mongoose');

const DesignClapsModelOpt = {
  name : {
    type: String,
    required: true,
    unique: true
  },
  claps:{
    type: Number,
  },

};

var DesignClaps = mongoose.model('DesignClaps' , DesignClapsModelOpt );

module.exports = { DesignClaps };
