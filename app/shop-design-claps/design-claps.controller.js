const { DesignClaps } = require('./design-claps.model.js');

const defaultClaps = [ 'solomon' , 'hearth' , 'circlesquare' ];

const createNewClap = (_name) => {
  return new DesignClaps({
    name: _name,
    claps: 0,
  }).save().then(_newDesign => {
    return _newDesign;
    // res.send( _newDesign );
  });
}

module.exports = function(app) {

  app.post('/design-claps/:name',  (req, res) => {

    if(!defaultClaps.includes(req.params.name)) {
      res.send({status: 'error' , message: 'wrong design name' });
      return;
    }

    DesignClaps.findOne( { name: req.params.name} ).then( _design => {
      if( !_design ){
        createNewClap(req.params.name).then(_newDesign => {
          res.send( _newDesign );
        })

      }else {
        const newClapAmount = _design.claps + 1;
        DesignClaps.updateOne({name: req.params.name } , {claps: newClapAmount} ).then((_data) => {

          res.send({ name: _design.name, claps:newClapAmount });

        }, (err) => {
          res.send(err);
        });
      }

    });

  });

  app.get('/design-claps',  (req, res) => {

    DesignClaps.find( ).then( _claps => {

      if( _claps.length == 0 ){
        _claps = [];
        for (var i = 0; i < defaultClaps.length; i++) {
          createNewClap(defaultClaps[i]);
        }
      }

      res.send(_claps);

    });

  });


};
