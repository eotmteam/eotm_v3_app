
const config = require('./auth.config.json');
const auth = require("./users.auth.js");
const { User, validate } = require("./users.model.js");
const bcrypt = require("bcrypt");

module.exports = function(app) {


  app.get('/user/current', auth, async(req, res) => {
    console.log('user current');
    res.send(req.user)
  })

  app.post('/user/current/logout', auth, async(req, res) => {
    console.log('user logout', req.token, req.user);
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save()
        res.send(true)
    } catch (error) {
        res.status(500).send(error)
    }

    // res.send(req.user)
  })

  app.post('/user/login', async(req, res) => {
    console.log('user login');
    try {
      const { email, password } = req.body
      let user = await User.findByCredentials(email, password)
      if (!user) {
        return res.status(401).send({error: 'Login failed! Check authentication credentials'})
      }
      const token = await user.generateAuthToken();
      res.send({ user, token })
    } catch (error) {
      res.status(400).send(error)
    }

  });

  app.post("/user/add", auth, async (req, res) => {
    console.log('add user');
    // validate the request body first
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //find an existing user
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(400).send("User already registered.");

    user = new User( req.body );

    const token = await user.generateAuthToken();
    await user.save();
    res.header("x-auth-token", token).send({ user, token});


  });

}
